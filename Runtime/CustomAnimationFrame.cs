﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace IfcEditorTypes
{
    public class CustomAnimationFrame
    {
        public float scale;
        public float time;
        public Vector3 position;
    }
}
