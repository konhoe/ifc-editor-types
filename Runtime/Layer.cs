﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace IfcEditorTypes
{
  /// <summary>
  /// Groups IFC Elements on a Layer to allow changing their visibility.
  /// </summary>
  public class Layer
  {
    public string name;

    /// <summary>
    /// Layers can be part of one or more LayerGroups which are peresented as Buttons in the Viewer Application
    /// </summary>
    public List<string> groups = new List<string>();

    [JsonIgnore]
    public HashSet<Transform> children = new HashSet<Transform>();

    public override bool Equals(object obj)
    {
      return obj is Layer layer &&
             name == layer.name;
    }

    public override int GetHashCode()
    {
      return 363513814 + EqualityComparer<string>.Default.GetHashCode(name);
    }
  }
}