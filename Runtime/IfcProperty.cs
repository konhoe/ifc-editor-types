﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace IfcEditorTypes
{
  /// <summary>
  /// Describes the IFC Attributes of a specific IFC Object identified by a unique ID.
  /// The Values of the Attributes are concatenated into a list of strings;
  /// </summary>
  public class IfcProperty
  {
    public string name;
    public string id;

    /// <summary>
    /// Defines wether this property was hidden in the IFC Editor
    /// </summary>
    public bool isActive = true;
    public List<String> attributes = new List<string>();
    [JsonIgnore]
    public bool isActiveGlobal = true;
  }
}