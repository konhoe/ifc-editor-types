﻿using System.Collections.Generic;

namespace IfcEditorTypes
{
    public class CustomAnimation
    {
        public string parentId;
        public string title;
        public List<string> triggerButtons;
        public bool loops;
        public List<CustomAnimationFrame> frames;
    }
}
