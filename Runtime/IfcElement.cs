﻿using System.Collections.Generic;

namespace IfcEditorTypes
{
  /// <summary>
  /// Represents a single IFC Element (eg IfcSlab).
  /// </summary>
  public class IfcElement
  {
    /// <summary>
    /// Unique id of a IfcElement, always 21 characters.
    /// </summary>
    public string id;
    public string elementName;
    public bool deleted;
    public List<IfcProperty> properties = new List<IfcProperty>();
    public List<Layer> layers = new List<Layer>();

    /// <summary>
    /// Optional User-defined Comment on this element. 
    /// </summary>
    public string comment;

  }
}