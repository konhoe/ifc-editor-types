﻿using System.Collections.Generic;

namespace IfcEditorTypes
{
  /// <summary>
  /// Combines all definitions and changes in an IFC Project to facilitate the Export to JSON
  /// </summary>
  public class IfcExport
  {
    public string version;

    /// <summary>
    /// A global comment for this Project.
    /// </summary>
    public string comment;
    public List<IfcElement> ifcElements = new List<IfcElement>();
    public List<IfcTransform> transforms = new List<IfcTransform>();
    public List<Annotation> annotations = new List<Annotation>();
    public List<CustomAnimation> customAnimations = new List<CustomAnimation>();
    public List<ExplosionAnimation> explosionAnimations = new List<ExplosionAnimation>();

    /// <summary>
    /// List of all Layers in this IFC Project
    /// </summary>
    /// <typeparam name="Layer"></typeparam>
    /// <returns></returns>
    public List<Layer> layers = new List<Layer>();

    /// <summary>
    /// List of all Buttons in this IFC Project, list index defines order of buttons in viewer.
    /// </summary>
    /// <typeparam name="string"></typeparam>
    /// <returns></returns>
    public List<string> buttons = new List<string>();

    /// <summary>
    /// Globally filtered IFC Properties. They should not be displayed in the viewer
    /// </summary>
    /// <typeparam name="string">Properties are identified by their name</typeparam>
    /// <returns></returns>
    public List<string> filteredProperties = new List<string>();

  }

}