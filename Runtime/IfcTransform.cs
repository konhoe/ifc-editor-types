﻿using System;
using UnityEngine;

namespace IfcEditorTypes
{
  /// <summary>
  /// Saves the applied transforms of each imported IFC file.
  /// </summary>
  public class IfcTransform
  {
    public IfcTransform() { }

    public IfcTransform(Transform t)
    {
      target = t.name;
      position = t.localPosition;
      rotation = t.localEulerAngles;
      scale = t.localScale;
    }

    /// <summary>
    /// The name of the IFC file which this transform concerns.
    /// </summary>
    public string target;
    public Vector3 position;
    public Vector3 rotation;
    public Vector3 scale;
  }
}