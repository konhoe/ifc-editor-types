﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IfcEditorTypes
{
    public class ExplosionAnimation
    {
        public string parentId;
        public string title;
        public float size;
        public float duration;
        public List<string> triggerButtons;
        public bool loops;
    }
}
