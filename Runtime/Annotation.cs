﻿using System.Collections.Generic;
using UnityEngine;

namespace IfcEditorTypes
{
  /// <summary>
  /// An Annotation is used to enrich the IFC model with further information outside of the IFC schema.
  /// </summary>
  public class Annotation
  {
    public string title;
    public string content;

    /// <summary>
    /// Name of the gameobject to which this Annotaion was attached to.
    /// </summary>
    public string parent;

    /// <summary>
    /// Visual representation of this Annotaion. (ball, flag, ...)
    /// </summary>
    public string visual;

    /// <summary>
    /// Can contain path to a media file (either jpeg, png or mp4)
    /// </summary>
    public string media;
    public List<Layer> layers = new List<Layer>();

    public Vector3 position;
    public float size;

    public Annotation()
    {
    }
  }
}